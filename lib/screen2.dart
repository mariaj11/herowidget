import 'package:flutter/material.dart';

class screen2 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Hero Widget"),),
      body: Center(
        child: Hero(
          tag: 'hero1',
          child: Container(
            child: Image.asset('assets/captain_america.png'),
            height: 150,
          ),
        ),
      ),
    );
  }
}