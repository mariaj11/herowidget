import 'package:flutter/material.dart';
import 'package:hero_widget/screen2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Hero Widget"),),
      body: GestureDetector(
        child: ListView.separated(
          padding: const EdgeInsets.only(top:20),
          itemCount: 10,
          
          itemBuilder: (_,index) => ListTile(        
            onTap: (){
              Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(seconds: 1) ,pageBuilder: (_,__,___)=>screen2()));
            },
            leading:Container(
              color: Colors.white,          
              child: Hero(
                tag: 'hero1',
                child: Container(
                  child: Image.asset('assets/captain_america.png'),
                  height: 150,
                ),
              ),          
            ),
            title: Text('Captain America'),
          ), separatorBuilder: (BuildContext context, int index) => Divider(color: Colors.blue),
        ),
      ),      
    );
  }
}
